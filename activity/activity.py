class Camper():
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type

    def career_track(self):
        print(f"Camper Name: {self.name}")
        print(f"Camper Batch: {self.batch}")
        print(f"Camper Course: {self.course_type}")

    def info(self):
        print(f"My name is {self.name} of batch {self.batch}")
        print(f"Currently enrolled in {self.course_type}")





zuitt_camper = Camper("Alan", "100", "python short course")

zuitt_camper.career_track()
zuitt_camper.info()