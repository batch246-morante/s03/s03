# [SECTION] List
# Lists are similar to arrays in JavaScript in a sense that they can contain collection of data.
# To create a list, the square brackers ([]) is used.


# Identifier
# Variable  --# Elements------------------
names = ["Dylan", "Windel", "Aira", "Jacob"] # Strings list
# index    0        1        2        3


programs = ["developer career", "main course", "short course"] # String list
durations = [260, 180, 20.52] # Number list / int or float
truth_values = [True, False, True, True, False] # Boolean list

sample_list = ["Apple", 3, False, "Potato", 4, True]
# Problems may arise, if you try to use lists with multiple types for something expects them all to be in same data type, it could have an error


# Getting the list size
# The number of elements in a list can be counted using the len() method
print(len(programs))

# Accessing Values
print(programs[0])


# Accessing the last item in the list
print(programs[-1])


# Access the second item in the list "durations"
print(durations[1])


# Access a range of values
# list[start index: end index - 1]
print(names[0:2])


# [SECTION] Mini exercise:
# 1. Create a list of names of 5 students
# 2. Create a list of grades for the 5 students
# 3. Use a loop to show student name with its grade

student_names = ["John", "Jane", "Joe", "Jack", "Jenny"]
student_grades = [98, 94, 96.5, 94, 90.5]

i = 0
while i < 5:
    print(f"Student name: {student_names[i]}, Student grade: {student_grades[i]}")
    i += 1


students = ["Joseph", "Gabriel", "Sernsern", "Paulo", "Ronie"]
grades = [99.9, 99.8, 99.89, 99.87, 99]

index = 0
while index < len(students):
    print(f"The grade of {students[index]} is {grades[index]}")
    index += 1


print(f"Current value: {programs[2]}")

# Update a list
programs[2] = "Short Courses"

print(f"The new value is: {programs[2]}")


# [SECTION] List Manipulation
# Adding list items
programs.append("Zumba")
print(programs)

# Delete the last item on the list using "del"
del programs[-1]
print(programs)


# Membership checks - the 'in' keyword checks if the element is in the list
print(20.52 in durations)
print(500 in durations)

# Sorting lists - sort()
names.sort()
print(names)


# [SECTION] Dictionary
# A dictionary is a collection which is ordered, changeable and does not allow duplicates.
# Ordered means that the items have a defined order, and that order will not change.
# Changeable means that values can be changed
# To create a dictionary, the curly braces ({}) is used and the key-value pairs are denoted with (key : value)

person1 = {
    "name" : "Brandon",
    "age" : 28,
    "occupation" : "student",
    "isEnrolled" : True,
    "subjects" : ["Python", "SQL", "Django"]
}

print(person1)

print(len(person1)) # to check the size or how many keys (properties) are there in an object

# Accessing values in the dictionary (we use square bracket [] to access key values in a dictionary)
print(person1["name"])

# The keys() method will return a list of all values in a dictionary
print(person1.keys())

# The values() method will return a list of all values in a dictionary
print(person1.values())

# The items() method will return each item in a dictionary, as key-value pairs in a list
print(person1.items())

# Adding key-value pairs can be done by either putting a new index key and assigning a value or update() method
person1["nationality"] = "Japanese"
person1.update({"fav_food" : "Isaw and Balut"})
print(person1)


# Delete entries can be done using the pop() method or del keyword
person1.pop("fav_food")
del person1["nationality"]
print(person1)

print(person1["subjects"][0])

person1.update({"contacts" : {"mobile" : "0999-888-777", "telephone" : "87000"}})
print(person1)

# Accessing a dictionary inside a dictionary
print("Dictionary inside dictionary: ")
print(person1["contacts"]["mobile"])

# ---------------------


# The clear() method empties the dictionary
person2 = {
    "name" : "John Doe",
    "age" : 18
    
}
print(person2)
person2.clear()
print(person2)


# Looping through dictionaries
for key in person1:
                        # key        value
    print(f"The value of {key} is {person1[key]}")



# Nested dictionaries - dictionaries can be nested inside each other
person3 = {
    "name" : "Mark",
    "age" : 15,
    "occupation" : "House husband",
    "isEnrolled" : True,
    "subjects" : ["Python", "SQL", "Django"]
}

classRoom = {
    "student1" : person1,
    "student2" : person3
}

print("CLASSROOM CONTENTS: ")
print(classRoom)


new_car = {
    "brand" : "Ford",
    "model" : "Mustang",
    "make_year" : 2023,
    "color" : "black"
}

print(f"I own a {new_car['brand']} {new_car['model']} and it was made in {new_car['make_year']}")

# [SECTION] Functions
# The "def" keyword is used to create a function. The syntax is:
# def function_name()

def my_greeting():
    print("Hello User!")

my_greeting()


# Parametes can be added to function to have more control what the inputs for the function will be
def greet_user(username): # << parameter (in the building of the function)
    print(f"hello, {username}!")

greet_user("Bob") # << argument (in the invocation) 


# Return statements - the "return" keyword allow functions to return values
def addition(num1, num2): # parameter
    return num1 + num2

sum = addition(5, 10) # arguments
print(f"The sum is {sum}")


# [SECTION] Lambda functions
# A lambda function is a small anonymous function that can be used for callbacks like arrow functions
# It is just like any normal python function, except that it has no name when defining it, and it is contained in one line of code.
# A lambda function can take any number of arguments, but can only have one expression.

greeting = lambda person : f"Hello {person}"
print(greeting("Elsie"))

# lambda function with multiple parameters
mult = lambda a,b : a * b
print(mult(5,6))


# Multiple condition in lamba function
                    # if --------------------------------else
divide = lambda a,b: "2nd number is not valid" if b == 0 else a/b
print(divide(7,0))
print(divide(10,2))


# [SECTION] Classes
# Classes would serve as blueprints to describe the concepts of objects
# To create a class, the "class" keyword is used along with the class name that starts with an uppercase

class Car():
    # properties that all car objects must have a defined method called init
    # __init__() method - when a class instance is created, the instance is automatically passed to the "self" parameter
    def __init__(self, brand, model, make_year):
        # creating properties and assigning
        self.brand = brand
        self.model = model
        self.make_year = make_year

        # other properties can be added and assigned hard-coded values
        self.fuel = "Gasoline"
        self.fuel_level = 0
        self.distance = 50

        # methods
    def fill_fuel(self):
        print(f"Current fuel level: {self.fuel_level}")
        print(f"filling up the tank...")
        self.fuel_level = 100
        print(f"New fuel level: {self.fuel_level}")

    def drive(self):
        print(f"The car has driven {self.distance} kilometers")
        print(f"The car's fuel level is {new_car3.fuel_level} - {self.distance}")

# Creating a new instance can be done by calling the class and providing the arguments
new_car2 = Car("Nissan", "GT-R", "2019")
new_car3 = Car("Ford", "Ranger", "2018")

# Displaying attributes can be done by using dot notation
print(f"My car is a new {new_car2.brand} {new_car2.model}")
print(f"My car is a new {new_car3.brand} {new_car3.model}")

# Calling methods of the instance
new_car3.fill_fuel()
# print(f"result is {new_car3}")

# new_car2.drive()